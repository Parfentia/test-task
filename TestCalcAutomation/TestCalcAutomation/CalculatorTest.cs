using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using System;
using System.IO;

namespace TestCalcAutomation
{
    [TestClass]
    public class CalculatorTest
    {
        private const string WindowsApplicationDriverUrl = "http://127.0.0.1:4723";
        private const string TestApp = "TestCalc.exe";
        protected static WindowsDriver<WindowsElement> AppSession;
        protected static WindowsDriver<WindowsElement> DesktopSession;

        [TestInitialize]
        public void TestInit()
        {
            var appiumOptions = new AppiumOptions();
            appiumOptions.AddAdditionalCapability("app", Path.Combine(Directory.GetCurrentDirectory(), TestApp));
            appiumOptions.AddAdditionalCapability("deviceName", "WindowsPC");
            appiumOptions.AddAdditionalCapability("platformName", "Windows");
            AppSession = new WindowsDriver<WindowsElement>(new Uri(WindowsApplicationDriverUrl), appiumOptions);
            Assert.IsNotNull(AppSession);
            Assert.IsNotNull(AppSession.SessionId);

            var optionsDesktop = new AppiumOptions();
            optionsDesktop.AddAdditionalCapability("app", "Root");
            optionsDesktop.AddAdditionalCapability("deviceName", "WindowsPC");
            optionsDesktop.AddAdditionalCapability("ms:experimental-webdriver", true);
            DesktopSession = new WindowsDriver<WindowsElement>(new Uri(WindowsApplicationDriverUrl), optionsDesktop);
        }

        [TestCleanup]
        public void Cleanup()
        {
            // Close the application and delete the session
            if (AppSession != null)
            {
                AppSession.Quit();
                AppSession = null;
            }
            // Close the application and delete the session
            if (DesktopSession != null)
            {
                DesktopSession.Quit();
                DesktopSession = null;
            }
        }

        [DataTestMethod]
        [DataRow("button11", "0")]
        [DataRow("button1", "1")]
        [DataRow("button2", "2")]
        [DataRow("button3", "3")]
        [DataRow("button4", "4")]
        [DataRow("button5", "5")]
        [DataRow("button6", "6")]
        [DataRow("button7", "7")]
        [DataRow("button8", "8")]
        [DataRow("button9", "9")]
        public void ClickOnNumber(string buttonId, string result)
        {
            AppSession.FindElementByAccessibilityId(buttonId).Click();
            var text = AppSession.FindElementByAccessibilityId("textBox1").Text;
            Assert.AreEqual(result, text);
        }

        [TestMethod]
        public void DivisionByZero()
        {
            AppSession.FindElementByAccessibilityId("button9").Click();
            AppSession.FindElementByAccessibilityId("button15").Click();
            AppSession.FindElementByAccessibilityId("button11").Click();
            AppSession.FindElementByAccessibilityId("button20").Click();
            var exception = false;
            try
            {
                if (AppSession.FindElementByName("����������") != null)
                {
                    exception = true;
                    AppSession.FindElementByName("�����").Click();
                    Assert.IsFalse(exception);
                }
            }
            catch
            {
                Assert.IsFalse(exception);
            }
        }

        [TestMethod]
        public void CheckInputCorrect()
        {
            AppSession.FindElementByAccessibilityId("textBox1").SendKeys("abc");
            AppSession.FindElementByAccessibilityId("button19").Click();
            var exception = false;
            try
            {
                if (AppSession.FindElementByName("����������") != null)
                {
                    exception = true;
                    AppSession.FindElementByName("�����").Click();
                }
                Assert.IsFalse(exception);
            }
            catch
            {
                Assert.IsFalse(exception);
            }
        }

        [DataTestMethod]
        [DataRow("button4", "button5", "button19", "9")]
        [DataRow("button8", "button4", "button18", "4")]
        [DataRow("button7", "button3", "button16", "21")]
        [DataRow("button8", "button2", "button15", "4")]
        public void CheckOperationsPositiveRange(string buttonA, string buttonB, string buttopOperetion, string result)
        {
            AppSession.FindElementByAccessibilityId(buttonA).Click();
            AppSession.FindElementByAccessibilityId(buttopOperetion).Click();
            AppSession.FindElementByAccessibilityId(buttonB).Click();
            AppSession.FindElementByAccessibilityId("button20").Click();
            var text = AppSession.FindElementByAccessibilityId("textBox1").Text;
            Assert.AreEqual(result, text);
        }

        [DataTestMethod]
        [DataRow("button4", "button2", "button19", "-2")]
        [DataRow("button5", "button4", "button18", "-9")]
        [DataRow("button1", "button8", "button16", "-8")]
        [DataRow("button4", "button1", "button15", "-4")]
        public void CheckSumNegative(string buttonA, string buttonB, string buttopOperetion, string result)
        {
            AppSession.FindElementByAccessibilityId(buttonA).Click();
            AppSession.FindElementByAccessibilityId("button10").Click();
            AppSession.FindElementByAccessibilityId(buttopOperetion).Click();
            AppSession.FindElementByAccessibilityId(buttonB).Click();
            AppSession.FindElementByAccessibilityId("button20").Click();
            var text = AppSession.FindElementByAccessibilityId("textBox1").Text;
            Assert.AreEqual(result, text);
        }


        [DataTestMethod]
        [DataRow("1.5", "1.3", "button19", "2.8")]
        [DataRow("3.75", "2.25", "button18", "1.5")]
        [DataRow("4.789", "10", "button16", "47.89")]
        [DataRow("8.25", "12", "button15", "0.6875")]
        public void CheckSumDesimal(string decimalA, string decimalB, string buttonOperation, string result)
        {
            AppSession.FindElementByAccessibilityId("textBox1").SendKeys(decimalA);
            AppSession.FindElementByAccessibilityId(buttonOperation).Click();
            AppSession.FindElementByAccessibilityId("textBox1").SendKeys(decimalB);
            AppSession.FindElementByAccessibilityId("button20").Click();
            var text = AppSession.FindElementByAccessibilityId("textBox1").Text;
            Assert.AreEqual(result, text);
        }

        [TestMethod]
        public void CheckMaxInput()
        {
            AppSession.FindElementByAccessibilityId("textBox1").SendKeys("1234567890");
            AppSession.FindElementByAccessibilityId("button16").Click();
            AppSession.FindElementByAccessibilityId("textBox1").SendKeys("10");
            AppSession.FindElementByAccessibilityId("button20").Click();
            var text = AppSession.FindElementByAccessibilityId("textBox1").Text;
            Assert.AreEqual("12345678900", text);
        }

        [TestMethod]
        public void CheckClearComand()
        {
            AppSession.FindElementByAccessibilityId("textBox1").SendKeys("15");
            AppSession.FindElementByAccessibilityId("button17").Click();
            var text = AppSession.FindElementByAccessibilityId("textBox1").Text;
            Assert.AreEqual("", text);
        }

        [TestMethod]
        public void CheckDeleteComand()
        {
            AppSession.FindElementByAccessibilityId("textBox1").SendKeys("15");
            AppSession.FindElementByAccessibilityId("button14").Click();
            var text = AppSession.FindElementByAccessibilityId("textBox1").Text;
            Assert.AreEqual("1", text);
        }
    }
}